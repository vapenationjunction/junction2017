# Junction Robot Example
## What the example is for
The provided cases are meant to show how the Viihde APIs can be called. 
Especially the user authentication might be a tricky proposition to start with no reference.

## How to use
First, make sure you have Python and pip installed (Python 2, unfortunately) 

Then, install Robot Framework
```shell
pip install robotframework
```
Open junction-cases.robot in your editor of choice and fill in the username and password that have been provided.

Run the cases with
```shell
robot --loglevel=debug junction-cases.robot
```
If all the stars are aligned, all cases should pass and a log file will be generated from which you can see more info.
