import logging

import requests


class JunctionLibrary(object):

    base_url_viihde_oauth             = 'https://rc-auth.elisaviihde.fi'
    base_url_rest_api                 = 'https://rc-rest-api.elisaviihde.fi'
    junction_client_id                = 'junction'
    junction_client_secret            = 'vfH7JQRZ82s5'

    #############
    # OAuth
    #############

    def get_authcode_from_oauth(self):
        payload = {"client_id": self.junction_client_id, "client_secret": self.junction_client_secret, "response_type": "code", "scopes": []}
        headers = {'content-type': 'application/json'}
        response = requests.post("{}/auth/authorize/access-code".format(self.base_url_viihde_oauth), json=payload, headers=headers)
        logging.debug(response.text)
        response.raise_for_status()
        data = Data(response.json())
        return data.code

    def get_oauth_token_for_user_with_password_and_code(self, username, password, code):
        payload = {'grant_type':'authorization_code','username': username, 'password':password,"client_id": self.junction_client_id, "code": code}
        headers = {'content-type': 'application/x-www-form-urlencoded'}
        response = requests.post("{}/auth/authorize/access-token".format(self.base_url_viihde_oauth), data=payload, headers=headers)
        logging.debug(response.text)
        response.raise_for_status()
        return Data(response.json())

    def get_oauth_token_for_user_with_refresh_token(self, refresh_token):
        payload = {'grant_type':'refresh_token',"client_id": self.junction_client_id, "client_secret": self.junction_client_secret, "refresh_token": refresh_token}
        headers = {'content-type': 'application/x-www-form-urlencoded'}
        response = requests.post("{}/auth/authorize/access-token".format(self.base_url_viihde_oauth), data=payload, headers=headers)
        logging.debug(response.text)
        response.raise_for_status()
        return Data(response.json())

    #############
    # Recordings
    #############

    def get_all_recordings(self, token, platform, app_version):
        headers = {'Authorization': 'Bearer '+ token}
        logging.debug(headers)
        response = requests.get("{}/rest/npvr/recordings/all?v=2&platform={}&appVersion={}".format(self.base_url_rest_api,platform,app_version), headers=headers)
        logging.debug(response.text)
        response.raise_for_status()
        return response.json()

    def get_npvr_url(self, token, program_id, protocol, platform, app_version):
        headers = {'Authorization': 'Bearer '+ token}
        logging.debug(headers)
        response = requests.get("{}/rest/npvr/recordings/url/{}?v=2&protocol={}&platform={}&appVersion={}".format(self.base_url_rest_api,program_id, protocol, platform, app_version), headers=headers)
        logging.debug(response.text)
        response.raise_for_status()
        return response.json()

    def get_recording_with_metadata(self, token, program_id):
        headers = {'Authorization': 'Bearer '+ token}
        logging.debug(headers)
        response = requests.get("{}/rest/npvr/recordings/info/{}?v=2&platform=online&appVersion=1.0&includeMetadata=true".format(self.base_url_rest_api, program_id), headers=headers)
        logging.debug(response.text)
        response.raise_for_status()
        return Data(response.json())

    #############
    # Search
    #############

    def do_search_for_user(self, token, search_string, include_metadata, platform, protocol):
        headers = {'Authorization': 'Bearer '+ token}
        logging.debug(headers)
        response = requests.get("{}/rest/search/query?q={}&includeMetadata={}&platform={}&protocol={}".format(self.base_url_rest_api, search_string, include_metadata, platform, protocol), headers=headers)
        logging.debug(response.text)
        response.raise_for_status()
        return response.json()

    #############
    # VOD
    #############

    def get_vod_categories(self, token):
        headers = {'Authorization': 'Bearer '+ token}
        logging.debug(headers)
        response = requests.get("{}/rest/vod/categories?v=1&protocol=hls,ss,rtsp".format(self.base_url_rest_api), headers=headers)
        logging.debug(response.text)
        response.raise_for_status()
        return response.json()

    def get_comedy_vods(self, token):
        headers = {'Authorization': 'Bearer '+ token}
        logging.debug(headers)
        response = requests.get("{}/rest/vod/categories/31/titles?v=1&includeMetadata=true".format(self.base_url_rest_api), headers=headers)
        logging.debug(response.text)
        response.raise_for_status()
        return response.json()

    def get_luokkakokous_2_vod(self, token):
        headers = {'Authorization': 'Bearer '+ token}
        logging.debug(headers)
        response = requests.get("{}/rest/vod/titles/134155?v=1&protocol=hls,ss,rtsp&includeMetadata=true".format(self.base_url_rest_api), headers=headers)
        logging.debug(response.text)
        response.raise_for_status()
        return response.json()

    def get_recommendation_for_vod_and_user(self, token, vod_id):
        headers = {'Authorization': 'Bearer '+ token}
        logging.debug(headers)
        response = requests.get("{}/rest/vod/recommended?v=1&similar_to={}&includeMetadata=true".format(self.base_url_rest_api, vod_id), headers=headers)
        logging.debug(response.text)
        response.raise_for_status()
        return response.json()

class Data(object):

    def __init__(self, data):
        data = {key: Data(value) if isinstance(value, dict) else value
                for key, value in data.items()}
        self.__dict__.update(data)