*** Settings ***
Library           JunctionLibrary.py

*** Variables ***
${username} =  TBD
${password} =  TBD


${id_token} =           empty
${token} =              empty
${refresh_token} =      empty

${program_id} =         7973414
${vod_id} =             152731

*** Testcases ***
Check that Viihde user can get token from OAuth
    ${code} =       Get authcode from OAuth
    ${response} =   Get OAuth token for user with password and code     ${username}    ${password}    ${code}
    Should be equal    ${response.response_type}    token
    Set Global Variable  ${token}  ${response.access_token}
    Set Global Variable  ${refresh_token}  ${response.refresh_token}

Fetch all recordings for user
    ${response} =  Get all recordings  ${token}    online    1.0

Retrieve NPVR URL
    ${response} =  Get NPVR URL   ${token}  ${program_id}   hls    online   1.0

Perform search
    ${response} =  Do search for user  ${token}    Down    true    online    hls

Get recording info
    ${response} =  Get recording with metadata  ${token}   ${program_id}

Get VOD categories
    ${response} =  Get VOD categories  ${token}

Get Comedy VODs
    ${response} =  Get Comedy VODs  ${token}

Get Luokkakokous 2 VOD
    ${response} =  Get Luokkakokous 2 VOD  ${token}

Get recommendations for VOD and user
    ${response} =  Get recommendation for VOD and user  ${token}   ${vod_id}


