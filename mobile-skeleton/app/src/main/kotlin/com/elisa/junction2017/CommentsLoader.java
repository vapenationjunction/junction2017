package com.elisa.junction2017;

import java.util.List;

public interface CommentsLoader {
    public void onUpdate(List<Comment> comments);
}
