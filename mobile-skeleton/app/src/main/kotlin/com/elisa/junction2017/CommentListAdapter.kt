package com.elisa.junction2017

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.bumptech.glide.Glide
import com.elisa.junction2017.service.Recording

typealias CardView = android.support.v7.widget.CardView

class CommentListAdapter(context: Context, private val recording: Recording, private var minute: Int) : BaseAdapter() {
    val apiClient = ApiClient()
    internal var sList = mutableListOf<Comment>()
    private val mInflator: LayoutInflater

    init {
        updateForMinute(minute)
        this.mInflator = LayoutInflater.from(context)
    }

    fun updateForMinute(newValue: Int) {
        minute = newValue
        apiClient.setCommentsLoader(recording.hashCode(), minute, { list ->
            sList = list
            notifyDataSetChanged()
        })
    }

    override fun getCount(): Int {
        return sList.size
    }

    override fun getItem(position: Int): Any {
        return sList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        val comment = sList[position]
        val view: View?
        val vh: ListRowHolder
        if (convertView == null) {
            view = this.mInflator.inflate(R.layout.list_row, parent, false)
            vh = ListRowHolder(view)
            view.tag = vh
        } else {
            view = convertView
            vh = view.tag as ListRowHolder
        }
        vh.username.text = comment.authorUsername
        vh.text.text = comment.content

        vh.reportSpoiler.setOnClickListener({
            Toast.makeText(view!!.context, "Spoiler reported!", Toast.LENGTH_SHORT).show()
            comment.downvote(recording, minute)
            notifyDataSetChanged() }
        )
        vh.reportNotSpoiler.setOnClickListener({
            Toast.makeText(view!!.context, "Not spoiler reported!", Toast.LENGTH_SHORT).show()
            comment.upvote(recording, minute)
            notifyDataSetChanged() }
        )

        vh.container.setCardBackgroundColor(view!!.resources.getColor(R.color.colorWhite))
        if (comment.isSpoiler()) {
            vh.container.setCardBackgroundColor(view.resources.getColor(R.color.spoiler))
        }

        vh.text.visibility = View.VISIBLE
        vh.imageView.visibility = View.GONE
        if (comment.isPicture) {
            vh.text.visibility = View.GONE
            vh.imageView.visibility = View.VISIBLE
            Glide.with(view.context)
                    .load(comment.content)
                    .into(vh.imageView)
        }
        return view
    }
}

private class ListRowHolder(row: View?) {
    val container: CardView = row?.findViewById<CardView>(R.id.comment_card_view) as CardView
    val username: TextView = row?.findViewById<TextView>(R.id.username) as TextView
    val text: TextView = row?.findViewById<TextView>(R.id.text) as TextView
    val reportSpoiler: Button = row?.findViewById<Button>(R.id.report_spoiler) as Button
    val reportNotSpoiler: Button = row?.findViewById<Button>(R.id.report_not_spoiler) as Button
    val imageView: ImageView = row?.findViewById<ImageView>(R.id.comment_image) as ImageView
}
