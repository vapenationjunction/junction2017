package com.elisa.junction2017.service

import android.content.Context
import android.os.Build
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import javax.inject.Inject

class AuthorizingInterceptor @Inject constructor(private val authorizer: Authorizer) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response = chain.proceed(inject(chain.request()))

    private fun inject(request: Request): Request =
            request.newBuilder().apply {
                authorizer.authorization()?.let { addHeader("Authorization", it) }
            }.build()
}

class PlatformInterceptor @Inject constructor(context: Context) : Interceptor {
    private val appVersion: String by lazy { context.packageManager.getPackageInfo(context.packageName, 0).versionName }

    override fun intercept(chain: Interceptor.Chain): Response = chain.proceed(inject(chain.request()))

    private fun inject(request: Request): Request = request.newBuilder()
            .url(request.url().newBuilder()
                         .addQueryParameter("platform", "online")
                         .addQueryParameter("appVersion", "2.0")
                         .build())
            .build()
}
