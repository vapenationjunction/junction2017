package com.elisa.junction2017;

import android.graphics.Bitmap;

public interface ImageLoader {
    public void onDownload(Bitmap image);
}
