package com.elisa.junction2017.service

import com.elisa.junction2017.extensions.logd
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.PublishSubject
import java.nio.channels.UnresolvedAddressException
import javax.inject.Inject

interface JunctionClient : LoginManager {
    val recordingsLoader: DataLoader<Recording>
    fun getRecordingUrl(programId: Long, deviceId: String): Single<String>
    fun resolve(url: String): Single<String>
    var filter: String?
}

class JunctionClientImpl @Inject constructor(private val auth: LoginManager,
                                             private val rest: RestApi) : JunctionClient, LoginManager by auth {
    override var filter: String? = null
        set(value) {
            val newValue = value?.trim().takeIf { it?.length ?: 0 > 1 }
            if (newValue != field) {
                logd("filter changed $field -> $newValue")

                field = newValue
                recordingsLoader.filter = field
            }
        }

    override val recordingsLoader = RecordingsLoader(rest::getRecordings)

    override fun getRecordingUrl(programId: Long, deviceId: String): Single<String> =
            rest.getRecordingUrl(programId, deviceId)
                    .map { it.url }
                    .observeOn(AndroidSchedulers.mainThread())

    override fun resolve(url: String): Single<String> =
            rest.resolve(url)
                    .flatMap {
                        val location = it.headers()["Content-Location"]
                        if (location != null) Single.just(location) else Single.error(UnresolvedAddressException())
                    }
                    .observeOn(AndroidSchedulers.mainThread())
}
