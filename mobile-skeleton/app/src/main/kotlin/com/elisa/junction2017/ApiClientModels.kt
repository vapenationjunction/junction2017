package com.elisa.junction2017

import com.elisa.junction2017.service.Recording

class Comment() {
    lateinit var key: String
    lateinit var authorUsername: String
    lateinit var content: String
    var upVotes: HashMap<String, String> = hashMapOf()
    var downVotes: HashMap<String, String> = hashMapOf()
    var isPicture: Boolean = false

    constructor(authorUsername: String,
                content: String,
                upVotes: HashMap<String, String>,
                downVotes: HashMap<String, String>,
                isPicture: Boolean) : this() {
        this.authorUsername = authorUsername
        this.content = content
        this.upVotes = upVotes
        this.downVotes = downVotes
        this.isPicture = isPicture
    }
}

fun Comment.isSpoiler() = this.upVotes.size - this.downVotes.size < 0

fun Comment.upvote(recording: Recording, minute: Int) {
    upVotes[_username] = ""
    ApiClient().upvoteComment(recording.hashCode(), minute, key, _username)
}

fun Comment.downvote(recording: Recording, minute: Int) {
    downVotes[_username] = ""
    ApiClient().downvoteComment(recording.hashCode(), minute, key, _username)
}

enum class ReactionType(val img: String) {
    HEART(""),
    WOW(""),
    SAD(""),
    // TODO(someone): add images paths and more reactions
}

class Reaction() {
    lateinit var type: ReactionType
    lateinit var authorUsername: String

    constructor(authorUsername: String, type: ReactionType): this() {
        this.type = type
        this.authorUsername = authorUsername
    }
}
