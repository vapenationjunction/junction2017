package com.elisa.junction2017.dagger

import android.app.SearchableInfo
import android.content.Context
import android.content.SharedPreferences
import com.elisa.junction2017.*
import com.elisa.junction2017.player.PlayerHolder
import com.elisa.junction2017.player.PlayerHolderImpl
import com.elisa.junction2017.service.*
import dagger.Component
import dagger.Module
import dagger.Provides
import dagger.android.AndroidInjector
import dagger.android.ContributesAndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AndroidSupportInjectionModule::class, AndroidBindingModule::class, JunctionModule::class))
interface JunctionComponent : AndroidInjector<JunctionApplication> {
    fun inject(junctionGlideModule: JunctionGlideModule)

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<JunctionApplication>() {
        abstract fun junctionModule(module: JunctionModule): Builder
    }
}

@Module
abstract class AndroidBindingModule {
    @ContributesAndroidInjector
    abstract fun mainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun loginFragment(): LoginFragment

    @ContributesAndroidInjector
    abstract fun contentFragment(): ContentFragment

    @ContributesAndroidInjector
    abstract fun playerActivity(): PlayerActivity

    @ContributesAndroidInjector
    abstract fun detailsActivity(): DetailsActivity
}

@Module
class JunctionModule(private val application: JunctionApplication) {
    @Provides
    @Singleton
    fun provideApplication() = application

    @Provides
    @Singleton
    fun provideApplicationContext(): Context = application

    @Provides
    @Singleton
    fun provideSharedPreferences(context: Context): SharedPreferences
            = context.getSharedPreferences("junction", Context.MODE_PRIVATE)

    @Provides
    @Singleton
    @Named("auth-prefs")
    fun provideAuthPreferences(context: Context): SharedPreferences
            = context.getSharedPreferences("auth", Context.MODE_PRIVATE)

    @Provides
    @Singleton
    fun provideAuthorizer(authorizer: AuthorizerImpl): Authorizer = authorizer

    @Provides
    @Singleton
    fun provideLoginManager(authorizer: Authorizer): LoginManager = authorizer

    @Provides
    @Singleton
    fun provideAuthApi(builder: Retrofit.Builder, @Named("okhttp") client: OkHttpClient): AuthApi = builder
            .client(client.newBuilder().cache(null).build())
            .baseUrl(BuildConfig.AUTH_URL)
            .build()
            .create(AuthApi::class.java)

    @Provides
    @Singleton
    fun provideRestApi(builder: Retrofit.Builder, @Named("okhttp-authed") client: OkHttpClient): RestApi = builder
            .client(client)
            .baseUrl(BuildConfig.REST_URL)
            .build()
            .create(RestApi::class.java)

    @Provides
    @Singleton
    fun provideRetrofitBuilder(): Retrofit.Builder = Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .addConverterFactory(MoshiConverterFactory.create())

    @Provides
    @Singleton
    @Named("okhttp")
    fun provideOkHttp(context: Context): OkHttpClient = OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC))
            .build()

    @Provides
    @Singleton
    @Named("okhttp-authed")
    fun provideOkHttpAuthed(@Named("okhttp") client: OkHttpClient,
                            authorizingInterceptor: AuthorizingInterceptor,
                            platformInterceptor: PlatformInterceptor): OkHttpClient = client
            .newBuilder()
            .addInterceptor(authorizingInterceptor)
            .addInterceptor(platformInterceptor)
            .build()

    @Provides
    @Singleton
    fun provideJunctionClient(client: JunctionClientImpl): JunctionClient = client

    @Provides
    @Singleton
    fun providePlayerHolder(playerHolder: PlayerHolderImpl): PlayerHolder = playerHolder
}