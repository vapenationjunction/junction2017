package com.elisa.junction2017;

import java.util.Map;

public interface ReactionsLoader {
    public void onUpdate(Map<ReactionType, Integer> map);
}
