@file:Suppress("unused")

package com.elisa.junction2017.extensions

import android.content.Context
import android.os.Parcel
import android.os.Parcelable
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.elisa.junction2017.BuildConfig

inline fun <reified T : Parcelable> createParcel(crossinline creator: (Parcel) -> T?): Parcelable.Creator<T> =
        object : Parcelable.Creator<T> {
            override fun createFromParcel(source: Parcel): T? = creator(source)
            override fun newArray(size: Int): Array<out T?> = arrayOfNulls(size)
        }

fun Parcel.readBoolean(): Boolean = readByte() == 1.toByte()

fun Parcel.writeBoolean(value: Boolean) {
    writeByte(if (value) 1 else 0)
}

inline fun <T> Parcel.readNullable(reader: (Parcel) -> T): T? = if (readBoolean()) reader(this) else null

inline fun <T> Parcel.writeNullable(value: T?, writer: (Parcel, T) -> Unit) {
    writeBoolean(value != null)
    value?.let { writer(this, it) }
}

inline fun <T, V> Parcel.writeMap(map: Map<T, V>,
                                  keyMapper: (Parcel, T) -> Unit,
                                  valueMapper: (Parcel, V) -> Unit) {
    writeInt(map.size)
    for ((key, value) in map) {
        keyMapper(this, key)
        valueMapper(this, value)
    }
}

inline fun <K, V> Parcel.readMap(keyReader: (Parcel) -> K, valueReader: (Parcel) -> V): Map<K, V> =
        mutableMapOf<K, V>().also {
            for (i in 0 until readInt()) {
                it.put(keyReader(this), valueReader(this))
            }
        }

fun ViewGroup.inflate(layout: Int, attachToRoot: Boolean = true): View = LayoutInflater.from(context).inflate(layout, this, attachToRoot)

fun Context.inflate(layout: Int): View = LayoutInflater.from(this).inflate(layout, null)

fun Boolean.toVisibility(gone: Int = View.GONE): Int = if (this) View.VISIBLE else gone

fun Any.logv(msg: String) {
    if (BuildConfig.DEBUG) {
        Log.v(javaClass.simpleName, msg)
    }
}

fun Any.logv(msg: String, throwable: Throwable) {
    if (BuildConfig.DEBUG) {
        Log.v(javaClass.simpleName, msg, throwable)
    }
}

fun Any.logd(msg: String) {
    if (BuildConfig.DEBUG) {
        Log.d(javaClass.simpleName, msg)
    }
}

fun Any.logd(msg: String, throwable: Throwable) {
    if (BuildConfig.DEBUG) {
        Log.d(javaClass.simpleName, msg, throwable)
    }
}

fun Any.logw(msg: String) {
    if (BuildConfig.DEBUG) {
        Log.w(javaClass.simpleName, msg)
    }
}

fun Any.logw(msg: String, throwable: Throwable) {
    if (BuildConfig.DEBUG) {
        Log.w(javaClass.simpleName, msg, throwable)
    }
}

fun Any.loge(msg: String) {
    if (BuildConfig.DEBUG) {
        Log.e(javaClass.simpleName, msg)
    }
}

fun Any.loge(msg: String, throwable: Throwable) {
    if (BuildConfig.DEBUG) {
        Log.e(javaClass.simpleName, msg, throwable)
    }
}

fun RecyclerView.isLast(view: View): Boolean = getChildAdapterPosition(view) == adapter.itemCount - 1
fun RecyclerView.isFirst(view: View): Boolean = getChildAdapterPosition(view) == 0
