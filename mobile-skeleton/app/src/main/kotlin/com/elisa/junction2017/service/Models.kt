@file:Suppress("MemberVisibilityCanPrivate")

package com.elisa.junction2017.service

import android.os.Parcel
import android.os.Parcelable
import com.elisa.junction2017.extensions.createParcel
import com.squareup.moshi.Json

data class AccessCodeRequest(@Json(name = "client_id") val clientId: String,
                             @Json(name = "client_secret") val clientSecret: String,
                             @Json(name = "response_type") val responseType: String = "code",
                             val state: String = "curlerman",
                             val scopes: List<String> = emptyList())

data class AccessCode(val code: String,
                      @Json(name = "expires_in") val expiresIn: Int,
                      val nonce: String,
                      val timestamp: Long,
                      val scopes: List<String>,
                      val state: String)

data class AccessToken(@Json(name = "access_token") val accessToken: String,
                       @Json(name = "refresh_token") val refreshToken: String,
                       @Json(name = "expires_in") val expiresIn: Long)

data class Recording(val programId: Long,
                     val name: String,
                     val description: String,
                     val channelId: Int,
                     val channelName: String,
                     val duration: Int,
                     val thumbnail: String) : Parcelable {

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeLong(programId)
        writeString(name)
        writeString(description)
        writeInt(channelId)
        writeString(channelName)
        writeInt(duration)
        writeString(thumbnail)
    }

    override fun describeContents(): Int = 0

    companion object {
        @JvmField
        @Suppress("unused")
        val CREATOR = createParcel { parcel ->
            Recording(parcel.readLong(),
                      parcel.readString(),
                      parcel.readString(),
                      parcel.readInt(),
                      parcel.readString(),
                      parcel.readInt(),
                      parcel.readString())
        }
    }
}

data class RecordingsResponse(val recordings: List<Recording>)
data class RecordingPlaybackUrl(val url: String)

enum class SearchHitType {
    @Json(name = "paytv")
    PAYTV,
    @Json(name = "epg")
    EPG,
    @Json(name = "svod")
    SVOD,
    @Json(name = "play")
    PLAY,
    @Json(name = "recording")
    RECORDING,
    @Json(name = "catchup")
    CATCHUP,
    @Json(name = "tvod")
    TVOD
}

data class SearchHit(val id: Int,
                     val title: String,
                     val description: String?,
                     val type: SearchHitType,
                     val score: Double,
                     val highlightedFields: HighlightedFields?,
                     val startTimeUTC: Long?,
                     val duration: Int?,
                     val source: String?,
                     val scrambled: Boolean?,
                     val imageUrl: String?,
                     val libraryId: Int?,
                     val genres: String?,
                     val protocols: String?,
                     val channelId: Int?,
                     val validFrom: Long?,
                     val validTo: Long?,
                     val price: String?)

data class HighlightedFields(val title: String?,
                             val description: String?)

data class SearchResult(val totalHitCount: Int,
                        val searchHits: List<SearchHit>)

data class SearchResults(val paytv: SearchResult,
                         val epg: SearchResult,
                         val svod: SearchResult,
                         val play: SearchResult,
                         val recording: SearchResult,
                         val catchup: SearchResult,
                         val tvod: SearchResult)

data class SearchResultResponse(val results: SearchResults)