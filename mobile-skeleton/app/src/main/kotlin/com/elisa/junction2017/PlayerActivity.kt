package com.elisa.junction2017

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.view.ViewCompat
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import com.elisa.junction2017.extensions.toVisibility
import com.elisa.junction2017.player.PlayerEngine
import com.elisa.junction2017.player.PlayerHolder
import com.elisa.junction2017.service.Recording
import dagger.android.support.DaggerAppCompatActivity
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.player.*
import kotlinx.android.synthetic.main.player_overlay.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

private const val CONTROLS_HIDE_DELAY = 3000L
private const val KEY_RECORDING = "recording"

class PlayerActivity : DaggerAppCompatActivity() {
    @Inject lateinit var holder: PlayerHolder

    private val recording by lazy { intent.getParcelableExtra<Recording>(KEY_RECORDING) }
    private lateinit var disposables: CompositeDisposable
    private val hider = Runnable { setControlsVisibility(false) }

    private var controlsVisibility: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN

        setContentView(R.layout.player)

        ViewCompat.setOnApplyWindowInsetsListener(root, { _, insets ->
            with(progress_area.layoutParams as ViewGroup.MarginLayoutParams) {
                leftMargin = insets.systemWindowInsetLeft
                bottomMargin = insets.systemWindowInsetBottom
                rightMargin = insets.systemWindowInsetRight
            }
            with(name.layoutParams as ViewGroup.MarginLayoutParams) {
                leftMargin = insets.systemWindowInsetLeft
                topMargin = insets.systemWindowInsetTop
            }
            insets
        })
        ViewCompat.requestApplyInsets(root)

        seek.setOnSeekBarChangeListener(object: SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                position.text = progress.toLong().formatTime()
                if (holder.player.seeking) {
                    seek(progress.toLong())
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {
                startSeeking()
            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {
                stopSeeking()
            }
        })

        toggle.setOnClickListener {
            if (holder.player.currentPlaybackState == PlayerEngine.PlaybackState.PLAYING) {
                // TODO(liza)
            }
            holder.player.toggle()

        }
        wth_button.setOnClickListener {
            holder.player.pause()
            setControlsVisibility(false)
            val commentFragment = CommentFragment.newInstance(
                    TimeUnit.MILLISECONDS.toMinutes(holder.player.currentPosition).toInt(), recording)
            supportFragmentManager.beginTransaction()
                    .replace(R.id.root, commentFragment)
                    .addToBackStack("WthComments")
                    .commit()
        }
        root.setOnClickListener { setControlsVisibility(!controlsVisibility) }
        debug_info.visibility = BuildConfig.DEBUG.toVisibility()

        name.text = recording.name
        val dur = TimeUnit.SECONDS.toMillis(recording.duration.toLong())
        seek.max = dur.toInt()
        duration.text = dur.formatTime()
        position.text = 0L.formatTime()
    }

    private var lastSeek: Long = 0
    private var seekPoint: Int = 0

    private val seekOverlayRevealer = Runnable {
        toggle.visibility = View.GONE
        seek_overlay.visibility = View.VISIBLE
    }

    private fun startSeeking() {
        holder.player.seeking = true
        lastSeek = System.currentTimeMillis() - holder.player.minimumScrubInterval
        seekPoint = seek.progress

        controls.postDelayed(seekOverlayRevealer, 150)
        setControlsVisibility(true)
    }

    private fun stopSeeking() {
        if (!holder.player.seeking) return

        controls.removeCallbacks(seeker)
        holder.player.seek(seek.progress.toLong())
        holder.player.seeking = false

        controls.removeCallbacks(seekOverlayRevealer)
        toggle.visibility = View.VISIBLE
        seek_overlay.visibility = View.GONE
        hideControls()
    }

    private val seeker = Runnable {
        lastSeek = System.currentTimeMillis()
        holder.player.seek(seek.progress.toLong())
    }

    private fun seek(position: Long) {
        controls.removeCallbacks(seeker)
        if (System.currentTimeMillis() - lastSeek > holder.player.minimumScrubInterval) {
            lastSeek = System.currentTimeMillis()
            holder.player.seek(position)
        } else {
            controls.postDelayed(seeker, System.currentTimeMillis() - lastSeek)
        }
        val d = position - seekPoint
        val s = TimeUnit.MILLISECONDS.toSeconds(Math.abs(d)) % 60
        val m = TimeUnit.MILLISECONDS.toMinutes(Math.abs(d)) % 60
        val h = TimeUnit.MILLISECONDS.toHours(Math.abs(d)) % 24
        seek_overlay.text = "${if (d < 0) "-" else "+"}${if (h > 0) "%1$02d:" else ""}%2$02d:%3$02d".format(h, m, s)
    }

    override fun onStart() {
        super.onStart()
        holder.player.prepare(recording.programId)
        holder.player.view = player_view

        disposables = CompositeDisposable(holder.player.position.subscribe {
            val length = holder.player.duration
            if (length > 0) {
                seek.max = length.toInt()
                duration.text = length.formatTime()
            }

            if (!holder.player.seeking) {
                seek.progress = it.toInt()
            }
            seek.secondaryProgress = holder.player.bufferedPosition.toInt()
        }, holder.player.playbackState.subscribe {
            if (it == PlayerEngine.PlaybackState.PLAYING) {
                hideControls()
            } else if (!it.isPlaying()) {
                setControlsVisibility(true)
            }

            root.keepScreenOn = it.isPlaying()
            toggle.setImageResource(if (it.isPlaying()) R.drawable.player_pause else R.drawable.player_play)
            wth_button.text = "WTH"
        }, holder.player.state.filter { it == PlayerEngine.State.DONE }.subscribe {
            finish()
        }, holder.player.info.subscribe {
            debug_info.text = it
        })
    }

    override fun onStop() {
        super.onStop()
        holder.takeIf { !isChangingConfigurations }?.release()
        controls.removeCallbacks(hider)
        controls.removeCallbacks(seeker)
        controls.removeCallbacks(seekOverlayRevealer)
        disposables.dispose()
    }

    private fun hideControls() {
        if (!holder.player.seeking) controls.postDelayed(hider, CONTROLS_HIDE_DELAY)
    }

    private fun setControlsVisibility(visible: Boolean, animate: Boolean = true) {
        Log.i("PlayerWth", "setControlVisibility($visible, $animate)")
        if (controls == null) return
        controls.removeCallbacks(hider)
        controlsVisibility = visible
        controls.animate()
                .alpha(if (visible) 1f else 0f)
                .setStartDelay(50).setDuration(if (animate) 200 else 0)
                .setListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationStart(animation: Animator) {
                        controls?.visibility = View.VISIBLE
                    }

                    override fun onAnimationEnd(animation: Animator) {
                        if (!visible) controls?.visibility = View.GONE
                    }
                })

        root.systemUiVisibility = when {
            controlsVisibility -> View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN

            else -> View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or
                    View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
                    View.SYSTEM_UI_FLAG_FULLSCREEN
        }
    }

    companion object {
        fun startPlayer(context: Context, recording: Recording) {
            context.startActivity(Intent(context, PlayerActivity::class.java).apply {
                putExtra(KEY_RECORDING, recording)
            })
        }
    }
}

private fun PlayerEngine.PlaybackState.isPlaying(): Boolean =
        this == PlayerEngine.PlaybackState.BUFFERING || this == PlayerEngine.PlaybackState.PLAYING

private fun Long.formatTime(): String = "%02d:%02d:%02d".format(TimeUnit.MILLISECONDS.toHours(this) % 24,
                                                                TimeUnit.MILLISECONDS.toMinutes(this) % 60,
                                                                TimeUnit.MILLISECONDS.toSeconds(this) % 60)
