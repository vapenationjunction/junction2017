package com.elisa.junction2017

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.util.DiffUtil
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.view.*
import com.elisa.junction2017.dagger.GlideApp
import com.elisa.junction2017.extensions.inflate
import com.elisa.junction2017.service.DataLoader
import com.elisa.junction2017.service.JunctionClient
import com.elisa.junction2017.service.Recording
import dagger.android.support.DaggerAppCompatActivity
import dagger.android.support.DaggerFragment
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_content.*
import kotlinx.android.synthetic.main.recording_item.view.*
import javax.inject.Inject

private const val TAG_FRAGMENT = "fragment"

class MainActivity : DaggerAppCompatActivity() {
    @Inject lateinit var client: JunctionClient
    private lateinit var disposable: Disposable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
    }

    override fun onStart() {
        super.onStart()

        val sharedPref = getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)
        _username = sharedPref.getString("username", "SOMETHING_WRONG")

        disposable = client.loggedIn.subscribe {
            val fragment = supportFragmentManager.findFragmentByTag(TAG_FRAGMENT)
            if (it && fragment !is ContentFragment) {
                supportFragmentManager.beginTransaction()
                        .replace(R.id.root, ContentFragment.newInstance())
                        .commit()
            } else if (fragment !is LoginFragment) {
                supportFragmentManager.beginTransaction()
                        .replace(R.id.root, LoginFragment.newInstance())
                        .commit()
            }
        }
    }

    override fun onStop() {
        super.onStop()
        disposable.dispose()
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)

        client.filter = intent.getStringExtra(SearchManager.QUERY)
        invalidateOptionsMenu()
    }
}

class ContentFragment : DaggerFragment() {
    @Inject lateinit var client: JunctionClient

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_content, container, false)

    private val contentAdapter by lazy { ContentAdapter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.main, menu)
        val menuItem = menu.findItem(R.id.menu_search)
        val searchView = menuItem.actionView as SearchView
        val searchManager = context.getSystemService(Context.SEARCH_SERVICE) as SearchManager
        searchView.setSearchableInfo(searchManager.getSearchableInfo(activity.componentName))
        searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean = query.length < 2
            override fun onQueryTextChange(newText: String): Boolean = false
        })

        menuItem.setOnActionExpandListener(object: MenuItem.OnActionExpandListener {
            override fun onMenuItemActionExpand(item: MenuItem): Boolean = true

            override fun onMenuItemActionCollapse(item: MenuItem): Boolean {
                client.filter = null
                return true
            }
        })

        val query = client.filter
        if (!query.isNullOrBlank()) {
            menuItem.expandActionView()
            searchView.setQuery(query, false)
            searchView.clearFocus()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        list.adapter = contentAdapter
        list.layoutManager = LinearLayoutManager(context)
    }

    override fun onStart() {
        super.onStart()

        contentAdapter.loader = client.recordingsLoader
    }

    override fun onStop() {
        super.onStop()

        contentAdapter.stop()
    }

    companion object {
        fun newInstance() = ContentFragment()
    }
}

class MovieHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
class ContentAdapter : RecyclerView.Adapter<MovieHolder>() {
    private val _data = mutableListOf<Recording>()
    private var data: List<Recording>
        get() = _data
        set(value) {
            if (_data == value) return

            val old = _data.toList()
            _data.clear()
            _data.addAll(value)
            val new = _data.toList()
            calculateDiff(old, new)
        }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: MovieHolder, position: Int) = with(holder.itemView) {
        val value = data[position]
        title.text = value.name
        GlideApp.with(cover).load(value.thumbnail).into(cover)
        tag = value
    }.also {
        if (data.size - 5 < position) {
            loader?.loadMore()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieHolder =
            MovieHolder(parent.inflate(R.layout.recording_item, false).apply {
                setOnClickListener {
                    DetailsActivity.showDetails(it.context, it.tag as Recording)
                }
                cover.viewTreeObserver.addOnPreDrawListener(object: ViewTreeObserver.OnPreDrawListener {
                    override fun onPreDraw(): Boolean {
                        cover.viewTreeObserver.removeOnPreDrawListener(this)
                        cover.layoutParams.height = cover.width * 9 / 16
                        return true
                    }
                })
            })

    fun stop() {
        disposable?.dispose()
    }

    private var disposable: Disposable? = null
    var loader: DataLoader<Recording>? = null
        set(value) {
            field = value

            stop()

            data = field?.data ?: listOf()
            disposable = field?.dataChanged?.subscribe { data = it }
            field?.takeIf { it.data.isEmpty() }?.loadMore()
        }

    private fun calculateDiff(old: List<Recording>, new: List<Recording>) {
        DiffUtil.calculateDiff(object: DiffUtil.Callback() {
            override fun areItemsTheSame(oldPosition: Int, newPosition: Int): Boolean =
                    old[oldPosition].programId == new[newPosition].programId

            override fun getOldListSize() = old.size

            override fun getNewListSize() = new.size

            override fun areContentsTheSame(oldPosition: Int, newPosition: Int): Boolean =
                    old[oldPosition] == new[newPosition]
        }).dispatchUpdatesTo(this)
    }
}
