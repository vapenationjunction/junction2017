package com.elisa.junction2017

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.elisa.junction2017.dagger.GlideApp
import com.elisa.junction2017.service.Recording
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_details.*

private const val KEY_RECORDING = "recording"

class DetailsActivity : DaggerAppCompatActivity() {

    private val recording by lazy { intent.getParcelableExtra<Recording>(KEY_RECORDING) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        setSupportActionBar(toolbar)

        GlideApp.with(this).load(recording.thumbnail).into(cover)
        cover.setOnClickListener {
            PlayerActivity.startPlayer(it.context, recording)
        }
        name.text = recording.name
    }

    companion object {
        fun showDetails(context: Context, recording: Recording) {
            context.startActivity(Intent(context, DetailsActivity::class.java).apply {
                putExtra(KEY_RECORDING, recording)
            })
        }
    }
}