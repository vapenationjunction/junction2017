package com.elisa.junction2017

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import com.elisa.junction2017.extensions.logd
import com.elisa.junction2017.service.JunctionClient
import dagger.android.support.DaggerFragment
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.fragment_login.*
import javax.inject.Inject

class LoginFragment : DaggerFragment() {
    @Inject lateinit var client: JunctionClient

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_login, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        password.setOnEditorActionListener { _, id, _ ->
            if (id == EditorInfo.IME_ACTION_DONE) {
                attemptLogin()
            }
            false
        }

        login.setOnClickListener { attemptLogin() }
    }

    private fun attemptLogin() {

        client.login(username.text.toString(), password.text.toString())
                .subscribeBy(onComplete = {
                    val sharedPref = activity.getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)
                    val editor = sharedPref.edit()
                    editor.putString("username", username.text.toString())
                    editor.apply()
                    _username = username.text.toString()
                    logd("logged in")
                }, onError = {
                    logd("login failed")
                })
    }

    companion object {
        fun newInstance() = LoginFragment()
    }
}
