package com.elisa.junction2017.service

import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject

private const val PAGE_SIZE = 10

abstract class DataLoader<T> {

    private val _data = mutableListOf<T>()
    val data: List<T>
        get() = _data.toList()

    private val dataChangedSubject: Subject<List<T>> = PublishSubject.create()
    val dataChanged: Observable<List<T>>
        get() = dataChangedSubject.observeOn(AndroidSchedulers.mainThread())

    private val isLoadingSubject: Subject<Boolean> = BehaviorSubject.createDefault(false)
    val isLoading: Observable<Boolean>
        get() = isLoadingSubject.observeOn(AndroidSchedulers.mainThread())
    var loading = false
        protected set(value) {
            if (field != value) {
                field = value
                isLoadingSubject.onNext(value)
            }
        }

    var hasMore = true
        private set

    protected var disposable: Disposable? = null

    abstract fun loadMore()

    protected fun dataLoaded(newData: List<T>) {
        if (newData.isNotEmpty()) {
            _data += newData
            dataChangedSubject.onNext(data)
        }
        loading = false
        hasMore = newData.size == PAGE_SIZE
    }

    fun reload() {
        disposable?.dispose()
        loading = false
        hasMore = true
        _data.clear()
        dataChangedSubject.onNext(data)
        loadMore()
    }

}

open class RecordingsLoader(private val source: (@ParameterName(name = "pageSize") Int,
                                                 @ParameterName(name = "pageOffset") Int,
                                                 @ParameterName(name = "filter") String?) -> Single<RecordingsResponse>) : DataLoader<Recording>() {
    override fun loadMore() {
        if (!loading && hasMore) {
            loading = true
            disposable = source(PAGE_SIZE, data.size, filter)
                    .map { it.recordings }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeBy(onSuccess = this::dataLoaded, onError = { loading = false })
        }
    }

    var filter: String? = null
        set(value) {
            if (value != field) {
                field = value
                reload()
            }
        }
}
