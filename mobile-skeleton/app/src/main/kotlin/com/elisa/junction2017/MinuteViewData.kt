package com.elisa.junction2017

data class MinuteViewData(val number: Int, val reactionsNum: Map<ReactionType, Int>)
