package com.elisa.junction2017.service

import android.content.SharedPreferences
import android.text.format.DateUtils
import com.elisa.junction2017.BuildConfig
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject
import javax.inject.Named

interface LoginManager {
    fun login(username: String, password: String): Completable
    val loggedIn: Observable<Boolean>
    fun logout()
}

interface Authorizer : LoginManager {
    fun authorization(): String?
}

private const val KEY_TOKEN = "authorization"
private const val KEY_REFRESH_TOKEN = "refresh_token"
private const val KEY_TOKEN_EXPIRES = "token_expires"

class AuthorizerImpl @Inject constructor(private val auth: AuthApi,
                                         @Named("auth-prefs") private val prefs: SharedPreferences) : Authorizer {

    private var token: String? = prefs.getString(KEY_TOKEN, null)
    private var refreshToken: String? = prefs.getString(KEY_REFRESH_TOKEN, null)
    private var expires = prefs.getLong(KEY_TOKEN_EXPIRES, 0)

    private val loggedInSubject = BehaviorSubject.createDefault<Boolean>(token != null)
    override val loggedIn: Observable<Boolean>
        get() = loggedInSubject.observeOn(AndroidSchedulers.mainThread())

    override fun login(username: String, password: String): Completable {
        return auth.getAccessCode(AccessCodeRequest(BuildConfig.CLIENT_ID, BuildConfig.CLIENT_SECRET))
                .flatMap { auth.getAccessToken(BuildConfig.CLIENT_ID, BuildConfig.CLIENT_SECRET, username, password, it.code) }
                .doOnSuccess {
                    saveToken(it)
                    loggedInSubject.onNext(true)
                }
                .doOnError { loggedInSubject.onNext(false) }
                .toCompletable()
    }

    override fun logout() {
        token = null
        refreshToken = null
        expires = 0

        prefs.edit()
                .remove(KEY_TOKEN)
                .remove(KEY_REFRESH_TOKEN)
                .remove(KEY_TOKEN_EXPIRES)
                .apply()
        loggedInSubject.onNext(false)
    }

    private fun saveToken(accessToken: AccessToken) {
        token = accessToken.accessToken
        refreshToken = accessToken.refreshToken
        expires = accessToken.expiresIn * 1000 + System.currentTimeMillis()

        prefs.edit()
                .putString(KEY_TOKEN, token)
                .putString(KEY_REFRESH_TOKEN, refreshToken)
                .putLong(KEY_TOKEN_EXPIRES, expires)
                .apply()
    }

    override fun authorization(): String? =
            when {
                expires - DateUtils.HOUR_IN_MILLIS > System.currentTimeMillis() -> token
                refreshToken != null -> synchronized(auth) {
                    if (expires - DateUtils.HOUR_IN_MILLIS > System.currentTimeMillis()) {
                        token
                    } else refreshToken?.let {
                        auth.getAccessToken(BuildConfig.CLIENT_ID, BuildConfig.CLIENT_SECRET, it)
                                .doOnSuccess(this::saveToken)
                                .doOnError { logout() }
                                .map { it.accessToken }
                                .blockingGet()
                    }
                }
                else -> null
            }?.let { "Bearer $it" }
}
