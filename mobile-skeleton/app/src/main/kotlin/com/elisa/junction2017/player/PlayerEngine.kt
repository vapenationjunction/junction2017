package com.elisa.junction2017.player

import android.content.Context
import android.content.SharedPreferences
import android.media.AudioManager
import android.net.Uri
import android.os.Handler
import android.os.Looper
import android.view.TextureView
import com.elisa.junction2017.extensions.logd
import com.elisa.junction2017.extensions.loge
import com.elisa.junction2017.service.JunctionClient
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.source.smoothstreaming.DefaultSsChunkSource
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.TrackSelectionArray
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout
import com.google.android.exoplayer2.upstream.*
import com.google.android.exoplayer2.util.MimeTypes
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.Observables
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject
import java.util.*
import javax.inject.Inject

interface PlayerHolder {
    val player: PlayerEngine
    fun release()
}

class PlayerHolderImpl @Inject constructor(private val context: Context,
                                           private val client: JunctionClient,
                                           private val sharedPreferences: SharedPreferences) : PlayerHolder {
    private var _player: PlayerEngine? = null
    override val player: PlayerEngine
        get() = _player ?: PlayerEngine(context, client, sharedPreferences).also {
            _player = it
        }

    override fun release() {
        _player?.release()
        _player = null
    }
}

class PlayerEngine(context: Context, private val client: JunctionClient, sharedPreferences: SharedPreferences) {
    enum class PlaybackState {
        IDLE, BUFFERING, PLAYING, PAUSED
    }

    enum class State {
        CREATED, PREPARING, READY, DONE, OPENING, RELEASED
    }

    private val uuid by lazy {
        sharedPreferences.getString("player_id", null) ?: UUID.randomUUID().toString().also {
            sharedPreferences.edit().putString("player_id", it).apply()
        }
    }
    private var disposable: Disposable? = null

    var currentPlaybackState: PlaybackState = PlaybackState.IDLE
        private set(value) {
            if (field != value) {
                logd("playback state $field -> $value")
                field = value
                playbackStateSubject.onNext(field)
                if (currentState == State.PREPARING && value == PlaybackState.PLAYING) {
                    currentState = State.READY
                }
            }
        }
    private val playbackStateSubject: Subject<PlaybackState> = BehaviorSubject.createDefault<PlaybackState>(currentPlaybackState)
    val playbackState: Observable<PlaybackState>
        get() = playbackStateSubject

    var currentPosition: Long = 0
        private set(value) {
            field = value
            currentPositionSubject.onNext(field)
        }
    private val currentPositionSubject: Subject<Long> = BehaviorSubject.create<Long>()
    val position: Observable<Long>
        get() = currentPositionSubject

    private val infoSubject: Subject<String> = BehaviorSubject.create<String>()
    val info: Observable<String>
        get() = infoSubject

    var currentState: State = State.CREATED
        private set(value) {
            if (field != value) {
                logd("player state $field -> $value")
                field = value
                stateSubject.onNext(value)
            }
        }
    private val stateSubject = BehaviorSubject.createDefault<State>(currentState)
    val state: Observable<State>
        get() = stateSubject

    val minimumScrubInterval: Int = 1000

    init {
        Observables.combineLatest(state, playbackState).subscribe {
            if (it.first == State.READY && it.second == PlaybackState.IDLE) {
                currentState = State.DONE
            }
        }
    }

    fun toggle() = when (currentPlaybackState) {
        PlaybackState.IDLE,
        PlaybackState.PAUSED -> play()
        PlaybackState.BUFFERING,
        PlaybackState.PLAYING -> pause()
    }

    private fun open() {
        currentState = State.OPENING
        disposable = client.getRecordingUrl(programId, uuid).flatMap { client.resolve(it) }.subscribeBy(onSuccess = {
            with(player) {
                source = SsMediaSource(Uri.parse(it), dataSourceFactory, DefaultSsChunkSource.Factory(dataSourceFactory), handler, null)
                if (!hasAudioFocus) {
                    requestFocus()
                }
                player.playWhenReady = true
                prepare(source)
            }
        }, onError = {})
    }

    private var programId: Long = -1

    fun prepare(programId: Long) {
        if (currentState != State.CREATED) return
        currentState = State.PREPARING
        this.programId = programId
        open()
    }

    private val handler = Handler(Looper.getMainLooper())
    private val bandwidthMeter by lazy { DefaultBandwidthMeter(handler, { _, _, _ -> updateBitrate() }) }
    private val httpDataSourceFactory by lazy { DefaultHttpDataSourceFactory("junction", bandwidthMeter) }
    private val dataSourceFactory: DataSource.Factory by lazy {
        DefaultDataSourceFactory(context, bandwidthMeter, httpDataSourceFactory)
    }
    private val player: SimpleExoPlayer by lazy {
        val loadControl = DefaultLoadControl(DefaultAllocator(true, C.DEFAULT_BUFFER_SEGMENT_SIZE), 70_000, 120_000, 2_000, 2_000)
        val selectionFactory = AdaptiveTrackSelection.Factory(bandwidthMeter)
        val selector = DefaultTrackSelector(selectionFactory)
        selector.parameters = DefaultTrackSelector.Parameters()
                .withViewportSizeFromContext(context, true)
        ExoPlayerFactory.newSimpleInstance(DefaultRenderersFactory(context), selector, loadControl).apply {
            addListener(componentListener)
            addVideoListener(componentListener)
        }
    }
    private var source: SsMediaSource? = null

    var duration: Long = 0
        get() = player.duration
        private set

    var bufferedPosition: Long = 0
        get() = player.bufferedPosition
        private set

    private var bitrate: Long = 0
        set(value) {
            if (value != field) logd("bitrate: $value")
            field = value
            updateInfo()
        }

    private val componentListener = ComponentListener()
    private val audioManager = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager

    private var hasAudioFocus: Boolean = false
    private var playbackStateWhenFocusLost = false
    private val audioFocusChangeListener = AudioManager.OnAudioFocusChangeListener {
        when (it) {
            AudioManager.AUDIOFOCUS_GAIN -> {
                hasAudioFocus = true
                player.volume = 1.0f
                player.playWhenReady = playbackStateWhenFocusLost
            }
            AudioManager.AUDIOFOCUS_LOSS, AudioManager.AUDIOFOCUS_LOSS_TRANSIENT -> {
                playbackStateWhenFocusLost = player.playWhenReady
                hasAudioFocus = false
                player.playWhenReady = false
            }
            AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK -> {
                playbackStateWhenFocusLost = player.playWhenReady
                player.volume = 0.15f
            }
        }
    }

    var view: AspectRatioFrameLayout? = null
        set(value) {
            field = value
            if (value != null) {
                val textureView: TextureView = (0 until value.childCount)
                        .firstOrNull { value.getChildAt(it) is TextureView }
                        ?.let { value.getChildAt(it) as TextureView }
                        ?: TextureView(value.context).also { value.addView(it, 0) }

                player.clearVideoTextureView(textureView)
                player.setVideoTextureView(textureView)
            } else {
                player.setVideoTextureView(null)
            }
        }

    private val updateProgressAction = Runnable { updateCurrentPosition() }

    var seeking: Boolean = false
        set(value) {
            field = value
            if (value) pause()
            else play()
        }

    private var videoWidth: Int = 0
    private var videoHeight: Int = 0

    private fun updateBitrate() {
        val trackSelections = player.currentTrackSelections
        (0 until (trackSelections?.length ?: 0))
                .map { trackSelections[it] }
                .forEach { trackSelection ->
                    trackSelection?.selectedFormat?.takeIf { MimeTypes.isVideo(it.sampleMimeType) }?.let {
                        bitrate = it.bitrate.toLong()
                    }
                }
    }

    private fun updateInfo() {
        infoSubject.onNext("%.2f Mbps\n${videoWidth}x$videoHeight\n${(bufferedPosition - currentPosition) / 1000}s".format(bitrate / 1024.0 / 1024.0))
    }

    fun release() {
        logd("release player")
        player.stop()
        @Suppress("DEPRECATION")
        audioManager.abandonAudioFocus(audioFocusChangeListener)
        hasAudioFocus = false
        disposable?.dispose()
        currentState = State.RELEASED
        handler.removeCallbacks(updateProgressAction)
        view = null
        player.release()
    }

    @Suppress("MemberVisibilityCanPrivate")
    fun pause() {
        player.playWhenReady = false
    }

    @Suppress("MemberVisibilityCanPrivate")
    fun play() {
        if (!hasAudioFocus) {
            requestFocus()
        }
        player.playWhenReady = true
    }

    private fun requestFocus() {
        @Suppress("DEPRECATION")
        hasAudioFocus = audioManager.requestAudioFocus(audioFocusChangeListener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN) == AudioManager.AUDIOFOCUS_REQUEST_GRANTED
    }

    private fun updateCurrentPosition() {
        handler.removeCallbacks(updateProgressAction)
        currentPosition = player.currentPosition
        updateInfo()
        if (player.playbackState != Player.STATE_IDLE && player.playbackState != Player.STATE_ENDED) {
            handler.postDelayed(updateProgressAction, 1000)
        }
    }

    fun seek(position: Long) = player.seekTo(position.coerceIn(0, duration - 500))

    private inner class ComponentListener : SimpleExoPlayer.VideoListener, Player.EventListener {
        override fun onRepeatModeChanged(repeatMode: Int) {
        }

        override fun onPlaybackParametersChanged(playbackParameters: PlaybackParameters) {
        }

        override fun onTracksChanged(trackGroups: TrackGroupArray, trackSelections: TrackSelectionArray) {
        }

        override fun onPlayerError(error: ExoPlaybackException) {
            loge("error", error)
        }

        override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
            currentPlaybackState = when (playbackState) {
                Player.STATE_IDLE, Player.STATE_ENDED -> PlaybackState.IDLE
                Player.STATE_BUFFERING -> if (playWhenReady || seeking) PlaybackState.BUFFERING else PlaybackState.PAUSED
                else -> if (playWhenReady || seeking) PlaybackState.PLAYING else PlaybackState.PAUSED
            }
            updateCurrentPosition()
        }

        override fun onLoadingChanged(isLoading: Boolean) {
        }

        override fun onPositionDiscontinuity() {
        }

        override fun onTimelineChanged(timeline: Timeline, manifest: Any?) {
        }

        override fun onVideoSizeChanged(width: Int, height: Int, unappliedRotationDegrees: Int, pixelWidthHeightRatio: Float) {
            videoWidth = width
            videoHeight = height
            updateInfo()
            view?.setAspectRatio(if (height == 0) 1f else width * pixelWidthHeightRatio / height)
        }

        override fun onRenderedFirstFrame() {
        }
    }

}

