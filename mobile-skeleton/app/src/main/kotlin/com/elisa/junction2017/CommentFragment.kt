package com.elisa.junction2017

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.elisa.junction2017.extensions.inflate
import com.elisa.junction2017.service.MinuteHolder
import com.elisa.junction2017.service.Recording
import kotlinx.android.synthetic.main.fragment_comment.*
import kotlinx.android.synthetic.main.fragment_comment.view.*
import kotlinx.android.synthetic.main.minute_item.view.*

class CommentFragment : Fragment() {
    private lateinit var recording: Recording
    private var minute: Int = 0
    private val apiClient = ApiClient()
    private lateinit var commentListAdapter: CommentListAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_comment, container, false)

        if (savedInstanceState != null) {
            recording = savedInstanceState.getParcelable("recording")
            minute = savedInstanceState.getInt("minute")
        }

        view.send_comment.setOnClickListener {
            Toast.makeText(view.context, "Comment has been sent", Toast.LENGTH_SHORT).show()
            apiClient.postComment(recording.hashCode(), minute, Comment(_username,
                    view.comment_form.text.toString(), hashMapOf(), hashMapOf(), false))
            view.comment_form.setText("")
            commentListAdapter.notifyDataSetChanged()
        }

        view.like_button.setOnClickListener {
            Toast.makeText(view.context, "<3", Toast.LENGTH_SHORT).show()
            apiClient.postReaction(recording.hashCode(), minute, Reaction(_username, ReactionType.HEART))
        }
        view.wow_button.setOnClickListener {
            Toast.makeText(view.context, "WOW", Toast.LENGTH_SHORT).show()
            apiClient.postReaction(recording.hashCode(), minute, Reaction(_username, ReactionType.WOW))
        }
        view.sad_button.setOnClickListener {
            Toast.makeText(view.context, ":(", Toast.LENGTH_SHORT).show()
            apiClient.postReaction(recording.hashCode(), minute, Reaction(_username, ReactionType.SAD))
        }

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        commentListAdapter = CommentListAdapter(view.context, recording, minute)
        commentsList.adapter = commentListAdapter
        timeline2.layoutManager = LinearLayoutManager(view.context, LinearLayoutManager.HORIZONTAL, false)
        timeline2.adapter = object: RecyclerView.Adapter<MinuteHolder>() {
            val data = (0..recording.duration / 60).map { min ->
                val map = hashMapOf(ReactionType.HEART to 0, ReactionType.WOW to 0,
                        ReactionType.SAD to 0)
                apiClient.setReactionsLoader(recording.hashCode(), min) { newMap ->
                    Log.d("KAPPA", "reactions loader");
                    map[ReactionType.HEART] = newMap[ReactionType.HEART]!!
                    map[ReactionType.WOW] = newMap[ReactionType.WOW]!!
                    map[ReactionType.SAD] = newMap[ReactionType.SAD]!!
                    notifyDataSetChanged()
                }
                MinuteViewData(min, map)
            }

            override fun onBindViewHolder(holder: MinuteHolder, position: Int) = with(holder.itemView) {
                val value = data[position]
                time.text = String.format("%d:%02d", position / 60, position % 60)
                heartsCnt.text = value.reactionsNum[ReactionType.HEART].toString()
                wowCnt.text = value.reactionsNum[ReactionType.WOW].toString()
                sadCnt.text = value.reactionsNum[ReactionType.SAD].toString()
                tag = value
            }

            override fun getItemCount(): Int {
                return data.size
            }

            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MinuteHolder {
                return MinuteHolder(parent.inflate(R.layout.minute_item, false))
            }
        }

        timeline2.addOnScrollListener(object: RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    val layoutManager = recyclerView.layoutManager as LinearLayoutManager
                    minute = layoutManager.findLastCompletelyVisibleItemPosition()
                    commentListAdapter.updateForMinute(minute)
                    Log.i("WthComments", "Set minute to $minute")
                }
            }
        })
        view.timeline2.scrollToPosition(minute - 3)
        commentsBackground.setOnClickListener {}
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putParcelable("recording", recording)
        outState.putInt("minute", minute)
    }

    companion object {
        fun newInstance(minute: Int, recording: Recording) = CommentFragment().apply{
            this.minute = minute
            this.recording = recording
        }
    }
}
