package com.elisa.junction2017

import com.elisa.junction2017.dagger.DaggerJunctionComponent
import com.elisa.junction2017.dagger.JunctionModule
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

class JunctionApplication : DaggerApplication() {
    val component by lazy { DaggerJunctionComponent.builder().junctionModule(JunctionModule(this)).create(this) as DaggerJunctionComponent }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> = component
}
