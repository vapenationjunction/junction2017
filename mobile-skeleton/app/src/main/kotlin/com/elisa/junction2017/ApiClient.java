package com.elisa.junction2017;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.function.Consumer;

public class ApiClient {
    private DatabaseReference database;
    private FirebaseStorage storage;
    private ValueEventListener commentsListener;

    public ApiClient() {
        database = FirebaseDatabase.getInstance().getReference();
        storage = FirebaseStorage.getInstance();
    }

    public void setCommentsLoader(int filmId, int minute, final CommentsLoader commentsLoader) {
        DatabaseReference commentsReference = database
                .child(Integer.toString(filmId))
                .child(Integer.toString(minute))
                .child("comments");
        if (commentsListener != null) {
            commentsReference.removeEventListener(commentsListener);
        }
        commentsListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<Comment> comments = new ArrayList<>();
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    Comment comment = child.getValue(Comment.class);
                    comments.add(comment);
                }
                commentsLoader.onUpdate(comments);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("DEBUG", "CommentsOnMinuteUpdate:onCancelled", databaseError.toException());
            }
        };
        commentsReference.addValueEventListener(commentsListener);
    }

    public void setReactionsLoader(int filmId, int minute, final ReactionsLoader reactionsLoader) {
        DatabaseReference reactionsReference = database
                .child(Integer.toString(filmId))
                .child(Integer.toString(minute))
                .child("reactions");
        ValueEventListener reactionsListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<ReactionType, Integer> reactionsCounter = new HashMap<>();
                reactionsCounter.put(ReactionType.HEART, 0);
                reactionsCounter.put(ReactionType.WOW, 0);
                reactionsCounter.put(ReactionType.SAD, 0);
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    ReactionType reactionType = child.getValue(ReactionType.class);
                    int oldNumber = reactionsCounter.get(reactionType);
                    reactionsCounter.put(reactionType, oldNumber + 1);
                }
                reactionsLoader.onUpdate(reactionsCounter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("DEBUG", "EmotionsOnMinute:onCancelled", databaseError.toException());
            }
        };
        reactionsReference.addValueEventListener(reactionsListener);
    }

    public void postComment(int filmId, int minute, Comment comment) {
        DatabaseReference databaseReference = database
                .child(Integer.toString(filmId))
                .child(Integer.toString(minute))
                .child("comments")
                .push();
        comment.setKey(databaseReference.getKey());
        databaseReference.setValue(comment);
    }

    public void postReaction(int filmId, int minute, Reaction reaction) {
        database.child(Integer.toString(filmId))
                .child(Integer.toString(minute))
                .child("reactions")
                .child(reaction.getAuthorUsername())
                .setValue(reaction.getType());
    }

    public void upvoteComment(int filmId, int minute, String commentId, String username) {
        database.child(Integer.toString(filmId))
                .child(Integer.toString(minute))
                .child("comments")
                .child(commentId)
                .child("upVotes")
                .child(username)
                .setValue("");
    }

    public void downvoteComment(int filmId, int minute, String commentId, String username) {
        database.child(Integer.toString(filmId))
                .child(Integer.toString(minute))
                .child("comments")
                .child(commentId)
                .child("downVotes")
                .child(username)
                .setValue("");
    }

    public void uploadImage(Bitmap image, String uploadPath) {
        StorageReference uploadReference = storage.getReference("images/" + uploadPath);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();
        uploadReference.putBytes(data);
    }

    public void downloadImage(String downloadPath, final ImageLoader imageLoader) {
        StorageReference downloadReference = storage.getReference("images/" + downloadPath);
        long maxSize = 1024 * 1024 * 1024;
        downloadReference.getBytes(maxSize).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                Bitmap image = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                imageLoader.onDownload(image);
            }
        });
    }
}
