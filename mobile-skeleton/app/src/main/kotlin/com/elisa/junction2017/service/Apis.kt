package com.elisa.junction2017.service

import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.*

interface AuthApi {
    @POST("auth/authorize/access-code")
    fun getAccessCode(@Body body: AccessCodeRequest): Single<AccessCode>

    @POST("auth/authorize/access-token")
    @FormUrlEncoded
    fun getAccessToken(@Field("client_id") clientId: String,
                       @Field("client_secret") clientSecret: String,
                       @Field("username") username: String,
                       @Field("password") password: String,
                       @Field("code") authCode: String,
                       @Field("grant_type") grantType: String = "authorization_code"): Single<AccessToken>

    @POST("auth/authorize/access-token")
    @FormUrlEncoded
    fun getAccessToken(@Field("client_id") clientId: String,
                       @Field("client_secret") clientSecret: String,
                       @Field("refresh_token") refreshToken: String,
                       @Field("grant_type") grantType: String = "refresh_token"): Single<AccessToken>
}

interface RestApi {
    @GET("rest/npvr/recordings?v=2.1")
    fun getRecordings(@Query("pageSize") pageSize: Int,
                      @Query("pageOffset") pageOffset: Int,
                      @Query("filter") filter: String? = null): Single<RecordingsResponse>

    @GET("rest/npvr/recordings/url/{id}?v=2.1")
    fun getRecordingUrl(@Path("id") programId: Long,
                        @Query("device_id") deviceId: String): Single<RecordingPlaybackUrl>

    @HEAD
    @Headers("Cache-Controls: no-cache")
    fun resolve(@Url url: String): Single<Response<Void>>

    @GET("rest/search/query")
    fun search(@Query("q") query: String): Single<SearchResultResponse>
}