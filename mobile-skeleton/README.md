# Mobile Skeleton

This application is written to act as an example on how to use Junction APIs on Android.
It is written in Kotlin, but the same libraries and techniques work in Java as well.

## Used libraries

* [Okhttp 3](http://square.github.io/okhttp/) HTTP client
* [Retrofit 2](https://square.github.io/retrofit/) type safe HTTP client
* [Moshi](https://github.com/square/moshi/) JSON library
* [Glide](https://github.com/bumptech/glide/) Image loading & caching library
* [Dagger 2](https://google.github.io/dagger/) Dependency injection
* [Exoplayer 2](https://github.com/google/ExoPlayer/) Media player
* [RxJava 2](https://github.com/ReactiveX/RxJava/) Reactive java
* [RxKotlin](https://github.com/ReactiveX/RxKotlin) Kotlin extensions for RxJava2
